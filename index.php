<?php

if (empty($_GET)) {
    die('No Parameters!');
}

require 'EtherScanApi.php';
require 'vendor/autoload.php';

$app_key = '{APP_KEY_HERE}';
$api = new EtherScanApi('https://api.etherscan.io/api', $app_key);

$raw_signed_transaction = $_GET['rti'];
$address = $_GET['address'];
if (isset($_GET['email'])) $email = $_GET['email'];
$start_time = str_replace('_', ' ', $_GET['start_time']);
$start_block = $_GET['start_block'];
$minimum = $_GET['minimum'];
if (substr($start_block, 0, 2) == '0x') {
    $start_block = hexdec($start_block);
}
$curr_time = gmdate('Y-m-d H:i:s');

// $api->getBlockNumber();
echo '<p>Current time: '.$curr_time.'</p>';
echo 'Start time: '.$start_time;
$block = $api->getBlockNumber();
echo '<p>Block Number: from api =>'.hexdec($block).' ,from user => '.$start_block.'</p>';
$balance = $api->getBalance($address);
echo '<p>Address balance: '.$balance.'</p>';
echo 'Minimum balance: '.$minimum;
$time_condition = (strtotime($curr_time) - strtotime($start_time) > 0) ;
$block_condition = (hexdec($block) >= $start_block);
$balance_condition = ($balance > $minimum);
echo '<p>Current time > Start time: '. ($time_condition ? 'YES' : 'NO') . '</p>';
echo 'Block >= Start block: '. ($block_condition ? 'YES' : 'NO');
echo '<p>Address balance > minimum: '. ($balance_condition ? 'YES' : 'NO').'</p>';

if (isset($_GET['testmode']) && $_GET['testmode'] == 1) {
    echo '<p>Test mode is ON.</p>';
    $mailer = new PHPMailer();
    $mailer->setFrom('noreply@test.com', 'Test mode');
    $mailer->Subject = 'Test Mailer';
    $mailer->addAddress($email, 'Mailer');
    $mailer->Body = 'All conditions are OK. Sending out test mail';
    if ($time_condition && $block_condition && $balance_condition) {
      $mailer->send();
    }
} else {
    echo '<p>Test mode is OFF.</p>';
    if ($time_condition && $block_condition && $balance_condition) {
      var_dump($api->broadcast($raw_signed_transaction));
    }
}
