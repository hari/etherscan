<?php

class EtherScanApi {

  private $api_url;
  private $token;

  public function __construct($base_url, $app_token) {
    $this->api_url = $base_url;
    $this->token = $app_token;
  }

  public function getBlock($number) {
    $query = sprintf("?module=block&action=getblockreward&blockno=%s&apikey=%s", $number, $this->token);
    $response = Requests::get($this->api_url.$query);
    return json_decode($response->body)->result;
  }

  public function getBlockNumber() {
    $query = sprintf("?module=proxy&action=eth_blockNumber&apikey=%s", $this->token);
    $response = Requests::get($this->api_url.$query);
    return json_decode($response->body)->result;
  }

  public function getBalance($address) {
    $query = sprintf("?module=account&action=balance&address=%s&tag=latest&apikey=%s", $address, $this->token);
    $response = Requests::get($this->api_url.$query);
    return json_decode($response->body)->result; 
  }

  public function broadcast($raw_signed_transaction) {
    $query = sprintf("?module=proxy&action=eth_sendRawTransaction&hex=%s&apikey=%s", $raw_signed_transaction, $this->token);
    $response = Requests::get($this->api_url.$query);
    return json_decode($response->body);
  }
  
}